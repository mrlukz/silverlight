﻿using System;

namespace SilverlightApplication1.Commands
{
    public class CommandManager
    {
        public static event EventHandler RequerySuggested;

        public static void InvalidateRequerySuggested()
        {
            if(RequerySuggested != null)
                RequerySuggested(null, EventArgs.Empty);
        }

    }
}
