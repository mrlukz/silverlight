﻿using SilverlightApplication1.Commands;
using System;
using System.ComponentModel;

namespace SilverlightApplication1.Model
{
    public class Employee : INotifyPropertyChanged, IDataErrorInfo
    {
        private string firstName;
        private string lastName;
        private string pesel;
        private DateTime birthDate;

        public Employee()
        {
            
        }

        public string FirstName
        {
            get { return firstName; }
            set 
            {
                firstName = value;
                OnPropertyChanged("FirstName");
                OnPropertyChanged("FullName");
            }
        }

        public string LastName
        {
            get { return lastName; }
            set 
            { 
                lastName = value;
                OnPropertyChanged("LastName");
                OnPropertyChanged("FullName");
            }
        }

        public string FullName
        {
            get { return FirstName + " " + LastName; }
        }

        public string Pesel
        {
            get { return pesel; }
            set 
            { 
                pesel = value;
                OnPropertyChanged("Pesel");
                OnPropertyChanged("BirthDate");
            }
        }

        public DateTime BirthDate
        {
            get { return birthDate; }
            set 
            { 
                birthDate = value;
                OnPropertyChanged("BirthDate");
            }
        }

        #region IDataErrorInfo
        string IDataErrorInfo.Error
        {
            get { return null; }
        }

        string IDataErrorInfo.this[string propertyName]
        {
            get { return GetValidationError(propertyName); }
        }

        private string GetValidationError(string propertyName)
        {
            string error = null;

            switch (propertyName)
            {
                case "FirstName":
                    error = ValidateName(FirstName);
                    break;

                case "LastName":
                    error = ValidateName(LastName);
                    break;

                case "Pesel":
                    error = ValidatePesel();
                    break;

                case "BirthDate":
                    error = ValidateBirthDate();
                    break;

            }

            return error;
        }

        static readonly string[] ValidatedProperties = { "FirstName", "LastName", "Pesel", "BirthDate" };

        public bool IsValid
        {
            get
            {
                foreach (string property in ValidatedProperties)
                {
                    if (GetValidationError(property) != null)
                    {
                        return false;
                    }
                }
                return true;

            }
        }

        private string ValidateBirthDate()
        {
            int day;
            int month;
            int year;

            if (ValidatePesel() == null && Int32.TryParse(Pesel.Substring(4, 2), out day) && Int32.TryParse(Pesel.Substring(2, 2), out month) && Int32.TryParse(Pesel.Substring(0, 2), out year))
            {
                if (month > 80 && month < 93)
                {
                    year += 1800;
                    month -= 80;
                }
                else if (month > 0 && month < 13)
                {
                    year += 1900;
                }
                else if (month > 20 && month < 33)
                {
                    year += 2000;
                    month -= 20;
                }
                else if (month > 40 && month < 53)
                {
                    year += 2100;
                    month -= 40;
                }
                else if (month > 60 && month < 73)
                {
                    year += 2200;
                    month -= 60;
                }

                if (year == BirthDate.Year && month == BirthDate.Month && day == BirthDate.Day)
                    return null;
                else
                    return "Data urodzin nie jest zgodna z numerem pesel!";
            }
            return null;
        }

        private string ValidatePesel()
        {
            if (Pesel != null && Pesel.Length == 11)
            {
                byte[] pesel = new byte[11];

                for (int i = 0; i < 11; i++)
                {

                    Byte tmpPeselDigit;
                    if (Byte.TryParse(Pesel.Substring(i, 1), out tmpPeselDigit))
                    {
                        pesel[i] = tmpPeselDigit;
                    }
                    else
                        return "Błędny numer pesel!";
                }

                int checkSum = 1 * pesel[0] +
                               3 * pesel[1] +
                               7 * pesel[2] +
                               9 * pesel[3] +
                               1 * pesel[4] +
                               3 * pesel[5] +
                               7 * pesel[6] +
                               9 * pesel[7] +
                               1 * pesel[8] +
                               3 * pesel[9];
                checkSum %= 10;
                checkSum = 10 - checkSum;
                checkSum %= 10;

                if (checkSum == pesel[10])
                    return null;
                else
                    return "Błędny numer pesel!";

            }
            else
                return "Pesel musi zawierać 11 znaków!";

        }

        private string ValidateName(string name)
        {
            if (String.IsNullOrWhiteSpace(name))
                return "Pole nie może być puste!";
            return null;
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            if(PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
            CommandManager.InvalidateRequerySuggested();
        }

        #endregion
    }
}
