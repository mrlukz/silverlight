﻿using System;
using System.Collections.Generic;

using SilverlightApplication1.Model;

namespace SilverlightApplication1.Services
{
    public static class DataProvider
    {
        public static List<Employee> GetEmployyes()
        {
            List<Employee> employeeList = new List<Employee>();

            employeeList.Add(new Employee
            {
                FirstName = "Jan",
                LastName = "Kowalski",
                Pesel = "62060627719",
                BirthDate = new DateTime(1962, 6, 6)
            });

            employeeList.Add(new Employee
            {
                FirstName = "Wojtek",
                LastName = "Nowak",
                Pesel = "80052190214",
                BirthDate = new DateTime(1980, 5, 21)
            });

            employeeList.Add(new Employee
            {
                FirstName = "Mariusz",
                LastName = "Czysty",
                Pesel = "89111310514",
                BirthDate = new DateTime(1989, 11, 13)
            });

            employeeList.Add(new Employee
            {
                FirstName = "Łukasz",
                LastName = "Macałek",
                Pesel = "62060627719",
                BirthDate = new DateTime(1962, 6, 6)
            });

            employeeList.Add(new Employee
            {
                FirstName = "Kamil",
                LastName = "Sabadarz",
                Pesel = "90101734319",
                BirthDate = new DateTime(1990, 10, 17)
            });

            employeeList.Add(new Employee
            {
                FirstName = "Agata",
                LastName = "Dąbek",
                Pesel = "83030307004",
                BirthDate = new DateTime(1983, 3, 3)
            });

            employeeList.Add(new Employee
            {
                FirstName = "Janusz",
                LastName = "Focht",
                Pesel = "70050569611",
                BirthDate = new DateTime(1970, 5, 5)
            });

            employeeList.Add(new Employee
            {
                FirstName = "Zofia",
                LastName = "Cofala",
                Pesel = "39011389202",
                BirthDate = new DateTime(1939, 1, 13)
            });

            employeeList.Add(new Employee
            {
                FirstName = "Magdalena",
                LastName = "Cwach",
                Pesel = "82031587400",
                BirthDate = new DateTime(1982, 3, 15)
            });

            employeeList.Add(new Employee
            {
                FirstName = "Katarzyna",
                LastName = "Zprzyszłości",
                Pesel = "58523011207",
                BirthDate = new DateTime(2158, 12, 30)
            });

            return employeeList;
        }
    }
}
