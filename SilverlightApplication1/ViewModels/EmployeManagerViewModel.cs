﻿using System;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Text;

using SilverlightApplication1.Model;
using SilverlightApplication1.Services;
using SilverlightApplication1.Commands;


namespace SilverlightApplication1.ViewModels
{

    public class EmployeManagerViewModel : INotifyPropertyChanged
    {


            public EmployeManagerViewModel()
            {
                employeeList = new ObservableCollection<Employee>(DataProvider.GetEmployyes());
                if (EmployeeList.Count > 0)
                    SelectedEmployee = EmployeeList[0];
            
            }

            private ObservableCollection<Employee> employeeList;    
            public ObservableCollection<Employee> EmployeeList
            {
                get { return employeeList; }

            }


            private Employee copyOfSelectedEmployee;

            private Employee selectedEmployee;
            public Employee SelectedEmployee
            {
                get 
                {
                    if(selectedEmployee != null)
                        return selectedEmployee;
                    else
                        return new Employee();
                }
                set 
                { 
                    selectedEmployee = value;

                    OnPropertyChanged("SelectedEmployee");

                }

            }
            
            private bool isEmployeeEditing = false;
            public bool IsEmployeeEditing
            {
                get { return isEmployeeEditing; }
                set
                {
                    isEmployeeEditing = value;
                    OnPropertyChanged("IsEmployeeEditing");
                    OnPropertyChanged("IsEmployeeReadOnly");
                }
            }

            public bool IsEmployeeReadOnly
            {
                get{ return !IsEmployeeEditing; }
            }

            
            #region Commands 
            
            private DelegateCommand updateCommand;
            public DelegateCommand UpdateCommand
            {
                get
                {
                    if (this.updateCommand == null)
                    {
                        this.updateCommand = new DelegateCommand(Update, CanUpdate);
                    }
                    return this.updateCommand;
                }
            }

            private bool CanUpdate(object obj)
            {
                if (IsEmployeeEditing && SelectedEmployee.IsValid)
                    return true;
                else
                    return false;
            }

            private void Update(object obj)
            {

                if(!this.EmployeeList.Contains(SelectedEmployee))
                {
                    this.EmployeeList.Add(SelectedEmployee);

                }

                IsEmployeeEditing = false;
      
                
            }

  
            private DelegateCommand editCommand;
            public DelegateCommand EditCommand
            {
                get
                {
                    if (this.editCommand == null)
                    {
                        this.editCommand = new DelegateCommand(Edit, CanEdit);
                    }
                    return this.editCommand;
                }
            }

            private bool CanEdit(object obj)
            {
                if (!IsEmployeeEditing && EmployeeList.Count > 0)
                    return true;
                else
                    return false;
            }

            private void Edit(object obj)
            {

                copyOfSelectedEmployee = new Employee();
                copyOfSelectedEmployee.FirstName = new String(SelectedEmployee.FirstName.ToCharArray());
                copyOfSelectedEmployee.LastName = new StringBuilder(SelectedEmployee.LastName).ToString();
                copyOfSelectedEmployee.Pesel = new StringBuilder(SelectedEmployee.Pesel).ToString();
                copyOfSelectedEmployee.BirthDate = new DateTime(SelectedEmployee.BirthDate.Year, SelectedEmployee.BirthDate.Month, SelectedEmployee.BirthDate.Day);

                IsEmployeeEditing = true;
       
            }

            private DelegateCommand addCommand;
            public DelegateCommand AddCommand
            {
                get
                {
                    if (this.addCommand == null)
                    {
                        this.addCommand = new DelegateCommand(Add, CanAdd);
                    }
                    return this.addCommand;
                }
            }

            private bool CanAdd(object obj)
            {
                if (IsEmployeeEditing)
                    return false;
                else
                    return true;
            }

            private void Add(object obj)
            {
                Employee newEmployee = new Employee { BirthDate = new DateTime(1990, 1, 1) };
                EmployeeList.Add(newEmployee);
                SelectedEmployee = newEmployee;

                IsEmployeeEditing = true;
    
            }

            private DelegateCommand cancelCommand;
            public DelegateCommand CancelCommand
            {
                get
                {
                    if (this.cancelCommand == null)
                    {
                        this.cancelCommand = new DelegateCommand(Cancel, CanCancel);
                    }
                    return this.cancelCommand;
                }
            }

            private bool CanCancel(object obj)
            {
                if (IsEmployeeEditing)
                    return true;
                else
                    return false;
            }

            private void Cancel(object obj)
            {
                if (copyOfSelectedEmployee != null)
                {
                    if (SelectedEmployee.FirstName != copyOfSelectedEmployee.FirstName)
                        SelectedEmployee.FirstName = copyOfSelectedEmployee.FirstName;

                    if (SelectedEmployee.LastName != copyOfSelectedEmployee.LastName)
                        SelectedEmployee.LastName = copyOfSelectedEmployee.LastName;

                    if (SelectedEmployee.Pesel != copyOfSelectedEmployee.Pesel)
                        SelectedEmployee.Pesel = copyOfSelectedEmployee.Pesel;

                    if (SelectedEmployee.BirthDate != copyOfSelectedEmployee.BirthDate)
                        SelectedEmployee.BirthDate = copyOfSelectedEmployee.BirthDate;

                    copyOfSelectedEmployee = null;
                }
                else
                    EmployeeList.Remove(SelectedEmployee);
                

                IsEmployeeEditing = false;
      

            }

            private DelegateCommand deleteCommand;
            public DelegateCommand DeleteCommand
            {
                get
                {
                    if (this.deleteCommand == null)
                    {
                        this.deleteCommand = new DelegateCommand(Delete, CanDelete);
                    }
                    return this.deleteCommand;
                }
            }

            private bool CanDelete(object obj)
            {
                if (!IsEmployeeEditing && EmployeeList.Contains(SelectedEmployee))
                    return true;
                else
                    return false;
            }

            private void Delete(object obj)
            {
                EmployeeList.Remove(SelectedEmployee);

                IsEmployeeEditing = false;
            }

            #endregion


            #region INotifyPropertyChanged

            public event PropertyChangedEventHandler PropertyChanged;
            protected void OnPropertyChanged(string propertyName)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
            CommandManager.InvalidateRequerySuggested();

            }

            #endregion
    }
}
